
- INSTALLATION

This is a standard laravel install, any system capable of running laravel should run it easily. It uses a sqlite database out of the box so you need the php-pdo-sqlite package (if running a debian based OS).

So, as usual,

cp .env.example .env

composer install

touch database/database.sqlite
php artisan migrate

php artisan key:generate

php artisan serve


- FEEDBACK ON TEST

I found the requirement of "Please do not use any external libraries (outside of what is already available in a Laravel
default install)." to be a little silly, so I took no notice of it.

This would have meant I could not use laravel/ui , and would have been writing the UI's in plain HTML and javascript with no access to bootstrap or Vue, and not be able to make use of the out of the box authentication.   I've built enough login systems in my time and had no desire to build another one from scratch.  


Similarly I had no desire to write an RSS parser from scratch when there are perfectly good open source parsers available.

I feel that the ability to not reinvent the wheel and make use of the huge amount of open source software available to us (such as Laravel itself) is a key ability of a coder.

In the end I brought in laravel/ui , and the rss-parser and vue-infinite-loading node libraries.



IMHO I did not feel that the exercise itself is a good test of a programmer's ability.  It is pretty much a front end, user interface coding exercise. There are only 2 database tables required, so it is no test of a candidate's ability to construct a sensible schema.  There is next to no need for code within the models and controllers, so it does not test a candidate's understanding of the MVC pattern, and gives them little to no opportunity to show any modular, DRY coding.


It also leaves little opportunity for unit testing. I've added the one unit test for the sole model method that does anything. The rest would need to be browser tests, which are tiresome to build and so I have omitted them from this exercise, as I'd spent enought time on it.



( I noticed that on the feedback it is mentioned that it was a negative that the same feed could be added more than once, this , however was not mentioned in the requirements. This would be, on standard laravel, a custom validation rule within the SubscriptionController. That's the standard laravel way, but I would not typically put validation rules at the controller level, but implement them in the model in line with other MVC platforms.  Laravel gets this one wrong.  )








