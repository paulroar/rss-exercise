<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Config;
use Tests\TestCase;
use App\Subscription;

class SubscriptionTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testProxiedAddress()
    {
    	Config::set('app.cors_proxy','noslash');
    	$subscription = new Subscription;
    	$subscription->address = 'add';
        $this->assertTrue($subscription->proxied_address == 'noslash/add');
    	Config::set('app.cors_proxy','noslash/');
        $this->assertTrue($subscription->proxied_address == 'noslash/add');
    }
}
