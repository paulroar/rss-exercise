@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">

		<form method="POST" action="{{ route('subscriptions.store') }}">
			@csrf
			<input type="text" name="address" maxlength="255" class="@error('address') is-invalid @enderror" value="{{ old('address') }}"/>
			@error('address')
			    <div class="alert alert-danger">{{ $message }}</div>
			@enderror
			<input type="submit" value="Add Feed"/>
		</form>

	</div>
</div>

<feed-scroll csrf="{{ csrf_token() }}"></feed-scroll>

@endsection