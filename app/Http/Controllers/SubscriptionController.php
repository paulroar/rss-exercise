<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;
use Auth;

class SubscriptionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Subscription::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subscription.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request,[
            'address' => 'required|url|max:255'
        ]);
        $subscription = new Subscription;
        $subscription->user_id = Auth::user()->id;
        $subscription->address = $validatedData['address'];
        $subscription->save();
        return redirect()->action('SubscriptionController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        return view('subscription.show',['subscription' => $subscription ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        $subscription->delete();
        return redirect()->action('SubscriptionController@index');
    }

    public function page(Request $request)
    {
        $this->authorize('viewAny',Subscription::class);
        $subscriptions = Auth::User()->subscriptions()->orderBy('id','desc')->simplePaginate(5);
        return response()->json($subscriptions);
    }
}
