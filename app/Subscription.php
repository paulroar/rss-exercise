<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Subscription extends Model
{
    protected $fillable = ['address'];
    protected $appends = ['proxied_address'];
	
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function getProxiedAddressAttribute()
    {

    	// lop the / off the end in case there's one in the config
    	
    	$proxy = preg_replace('/\/$/','',config('app.cors_proxy'));

    	return $proxy . '/' . $this->address;
    }
}
